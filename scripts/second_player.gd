extends Spatial

var position = Transform()
var checked = true

# Called when the node enters the scene tree for the first time.
func _ready():
	position = global_transform
	randomize()
	$main_mesh.mesh.material.albedo_color.h = randf()

func _set_pos(pos):
	#global_transform = pos
	position = pos
	checked = true

func _process(delta):
	global_transform.basis = position.basis
	global_transform.origin.x = lerp(global_transform.origin.x, position.origin.x, 0.1)
	global_transform.origin.y = lerp(global_transform.origin.y, position.origin.y, 0.1)
	global_transform.origin.z = lerp(global_transform.origin.z, position.origin.z, 0.1)
	get_node("/root/world/player/Label2").text = str(checked)
	checked = false
	
