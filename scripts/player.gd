extends KinematicBody

#состояния
enum {IDLE, RUN, WALL_RUN, FALLING, HOOK}
var state = IDLE
var can_walljump = true

var start_spd = 7 #стартовая скорость
var max_spd = 15 #максимальная скррость
var spd = start_spd # текущая скорость
var acceleration = 15.0 / 600#ускорение
var mouse_sensitivity = 0.1 #чувствительность мыши
var vel = Vector3(0,0,0) #вектор движения
var mouse_delta = Vector2() #хранит в себе перемещение мышки
var hp = 100 #хп игрока
var hits = 0 #попадания
var hook_vel = 0
var hook_point = Vector3()
var new_hook

#анимация камеры
var cam_shake = 0 
var r
var n
var cam_run = 0

#ноды
var cam_id


func _ready():
	cam_id = $Camera
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	randomize()


func _physics_process(delta):
	var forward = transform.basis.z #базисный вектор по z относительно игрока
	var right = transform.basis.x #базисный вектор по x относительно игрока
	var is_on_floor = $RayCast.is_colliding() or is_on_floor()
	
	#обработка состояний
	if is_on_floor and not state == HOOK and not Input.is_action_pressed("move_forward"):
		state = IDLE
	if is_on_floor and not state == HOOK and Input.is_action_pressed("move_forward"):
		state = RUN
	if not is_on_floor() and not state == HOOK:
		state = FALLING
	if is_on_wall() and not state == HOOK and not is_on_floor and Input.is_action_pressed("move_forward"):
		state = WALL_RUN
	
	
	if is_on_floor:
		vel.x = 0
		vel.z = 0
		can_walljump = true
	
	if state == FALLING:
		vel.y -= 20 * delta
	
	if state == WALL_RUN:
		if vel.y > 0:
			vel.y -= 7 * delta
		else:
			vel.y -= 1 * delta
	
	if state != HOOK and is_on_floor():
		vel.y = -20 * delta
	
	if state == HOOK:
		var hook_velocity = $Camera.global_transform.origin.direction_to(hook_point) * hook_vel
		hook_vel += 1
		vel.x = lerp(vel.x, hook_velocity.x, 0.01)
		vel.y = lerp(vel.y, hook_velocity.y, 0.01)
		vel.z = lerp(vel.z, hook_velocity.z, 0.01)
		vel.y -= 20 * delta
	
	#сброс скорости
	if not Input.is_action_pressed("move_forward"):
		spd = lerp(spd, start_spd, 0.05)
	
	
	#управление
	if Input.is_action_pressed("move_forward") and is_on_floor:
		vel += forward * -spd
		spd = clamp(spd + acceleration, start_spd, max_spd)
	
	if Input.is_action_pressed("move_backward") and is_on_floor:
		vel += forward * spd
	
	if Input.is_action_pressed("move_left") and is_on_floor:
		vel += right * -spd
	
	if Input.is_action_pressed("move_right") and is_on_floor:
		vel += right * spd
	
	#прыжок от пола
	if (state == IDLE or state == RUN) and Input.is_action_pressed("jump"):
		vel.y = 9
	
	#толкание в стену
	#if state == WALL_RUN:
	#	move_and_slide(get_slide_collision(0).normal * -(spd-1), Vector3(0,1,0))
	
	#прыжок от стены
	if state == WALL_RUN and can_walljump and Input.is_action_just_pressed("jump"):
		if forward.angle_to(get_slide_collision(0).normal) > PI/2:
			vel = forward * -10
			vel.y = 9
		if forward.angle_to(get_slide_collision(0).normal) < PI/8:
			vel = forward * -1
			vel.y = 9
			can_walljump = false
	
	#анимация бега по стенам
	if state == WALL_RUN:
		var vec1 = Vector2(get_slide_collision(0).normal.x, get_slide_collision(0).normal.z)
		var vec2 = Vector2(forward.x, forward.z)
		if vec1.angle_to(vec2) <= 0:
			cam_id.rotation_degrees.z = lerp(cam_id.rotation_degrees.z, 15, 0.1)
		else:
			cam_id.rotation_degrees.z = lerp(cam_id.rotation_degrees.z, -15, 0.1)
	else:
		cam_id.rotation_degrees.z = lerp(cam_id.rotation_degrees.z, 0, 0.1)
	
	move_and_slide(vel, Vector3(0,1,0))
	
	
	"""
	Анимация камеры
	"""
	#анимация камеры при приземлении
	if is_on_floor() and vel.y < -1:
		cam_shake = 1
		n = 0.1
		r = clamp(-vel.y / 20, 0.3, 1.9)
	
	if cam_shake == 1:
		cam_id.translation.y = lerp(cam_id.translation.y, 2 - r, n)
		n+= 1.0 / 60
		if cam_id.translation.y < 2 - r + 0.1:
			cam_shake = 2
			n = 0
	
	if cam_shake == 2:
		n+= 1.0 / 60
		cam_id.translation.y = lerp(cam_id.translation.y, 2, n)
		if cam_id.translation.y > 1.9:
			cam_shake = 0
	
	#анимация бега
	if state == RUN and abs(cam_id.rotation_degrees.z) < 0.8:
		var angular_spd = clamp(spd / 80.0, PI/50, PI/20)
		var rad = clamp(spd * 0.04, 0.2, 1)
		cam_id.rotation_degrees.z = sin(cam_run) * rad
		cam_run += angular_spd
	else:
		cam_run = 0
	
	
	#поворот камеры мышкой
	cam_id.rotation_degrees.x -= mouse_delta.y * mouse_sensitivity
	rotation_degrees.y -= mouse_delta.x * mouse_sensitivity
	
	cam_id.rotation_degrees.x = clamp(cam_id.rotation_degrees.x,-80,80)
	
	mouse_delta.y = 0
	mouse_delta.x = 0
	
	
func _process(delta):
	
	if Input.is_action_just_pressed("hook"):
		if $Camera/hook_raycast.is_colliding():
			hook_point = $Camera/hook_raycast.get_collision_point()
			hook_vel = 0
			state = HOOK
			
			new_hook = preload("res://scenes/hook_object.tscn").instance()
			add_child(new_hook)
			new_hook.point2 = hook_point
	
	if Input.is_action_just_released("hook") and state == HOOK:
		state = FALLING
		new_hook.queue_free()
	
	if Input.is_action_just_pressed("fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen
	
	if Input.is_action_just_pressed("restart"):
		restart()
	
	if Input.is_action_just_pressed("mouse_mode"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	"""
	if Input.is_action_just_pressed("ui_up"):
		translation.y += 10
	"""
	#уведомление о столкновении
	for i in get_node("/root/world").objects_id:
		if global_transform.origin.distance_squared_to(i.global_transform.origin) < 10:
			_notify()
	
	if hp <= 0:
		restart()
	
	if translation.y < -10:
		restart()
	
	$Label.modulate.a -= 0.01
	$Label3.text = "HP:" + str(hp)
	$Label4.text = "HITS:" + str(hits)
	
	
func _input(event):
	#получаем сдвиг мыши
	if event is InputEventMouseMotion:
		mouse_delta = event.relative

func restart():
	hp = 100
	translation.x = rand_range(0,250)
	translation.z = rand_range(0,150)
	translation.y = 50
	vel = Vector3(0,0,0)

func _on_create_pressed():
	get_node("/root/world").server_create()
	$create.queue_free()
	$connect.queue_free()
	$LineEdit.queue_free()


func _on_connect_pressed():
	get_node("/root/world").server_connect()
	$create.queue_free()
	$connect.queue_free()
	$LineEdit.queue_free()

func _notify():
	$Label.modulate.a = 1
