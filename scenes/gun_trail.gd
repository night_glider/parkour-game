extends MeshInstance


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass # Replace with function body.



func _process(delta):
	mesh.material.albedo_color.a8 -= 2
	if mesh.material.albedo_color.a8 < 0:
		queue_free()
