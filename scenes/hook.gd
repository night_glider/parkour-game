extends KinematicBody

var fly = false
var spd
var default_position

func shoot(pos1, pos2):
	global_transform.origin = pos1
	spd = pos1.direction_to(pos2)
	fly = true

func reset():
	translation = default_position
	fly = false

# Called when the node enters the scene tree for the first time.
func _ready():
	default_position = translation


func _physics_process(delta):
	move_and_slide(spd, Vector3(0,1,0))
	
	
