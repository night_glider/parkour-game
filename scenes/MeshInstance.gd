tool
extends MeshInstance


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	var point2 = get_parent().get_node("hook").transform
	transform = transform.looking_at(point2.origin, Vector3(0,1,0))

