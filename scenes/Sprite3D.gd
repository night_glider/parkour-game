tool
extends Sprite3D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	region_rect.size = Vector2(20, get_parent().get_node("hook").global_transform.origin.distance_to(global_transform.origin) * 64 * 1.5)
	var point2 = get_parent().get_node("hook").global_transform.origin
	var point1 = global_transform.origin

	global_transform = global_transform.looking_at(point2,Vector3(0,1,0))
	
