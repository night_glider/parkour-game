extends Spatial

var point2 : Vector3

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



func _process(delta):
	global_transform = global_transform.looking_at(point2, Vector3(0,1,0))
	scale.z = global_transform.origin.distance_to(point2)
