extends Spatial

export var throwback = 0.05

var default_pos : Vector3
var focus_pos : Vector3
var current_pos : Vector3

# Called when the node enters the scene tree for the first time.
func _ready():
	default_pos = translation
	focus_pos = Vector3(translation.x - 0.16, translation.y + 0.1, translation.z + 0.4)

func shoot():
	if current_pos == default_pos:
		translation.z += throwback
	else:
		translation.z += throwback/5
	
	rotation_degrees.x += 1/2.0
	rotation_degrees.y += rand_range(-1,1)/2.0
	rotation_degrees.z += rand_range(-1,1)/2.0
	$Particles.emitting = true
	
	var new_gun_trail = preload("res://scenes/gun_trail.tscn").instance()
	get_node("/root/world").add_child(new_gun_trail)
	new_gun_trail.global_transform = $gun_barrel.global_transform
	get_node("/root/world").create_trail($gun_barrel.global_transform)
	
	if $gun_barrel/RayCast.is_colliding():
		get_node("/root/world").damage_player( int( $gun_barrel/RayCast.get_collider().get_parent().get_name() ) )

func focus():
	pass

func _process(delta):
	
	if Input.is_action_pressed("LMB"):
		shoot()
	else:
		$Particles.emitting = false
		pass
	
	if Input.is_action_pressed("RMB"):
		current_pos = focus_pos
	else:
		current_pos = default_pos
	
	translation.x = lerp(translation.x, current_pos.x, 0.1)
	translation.y = lerp(translation.y, current_pos.y, 0.1)
	translation.z = lerp(translation.z, current_pos.z, 0.1)
	rotation_degrees.x = lerp(rotation_degrees.x, 0, 0.1)
	rotation_degrees.y = lerp(rotation_degrees.y, 0, 0.1)
	rotation_degrees.z = lerp(rotation_degrees.z, 0, 0.1)
	
	

	
