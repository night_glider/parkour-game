extends Camera


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	if Input.is_action_just_released("zoom_down"):
		fov = clamp(fov+5, 10, 70)
	if Input.is_action_just_released("zoom_up"):
		fov = clamp(fov-5, 10, 60)
