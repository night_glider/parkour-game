extends Spatial

export var throwback = 0.05 * 10

var default_pos : Vector3
var focus_pos : Vector3
var current_pos : Vector3

# Called when the node enters the scene tree for the first time.
func _ready():
	default_pos = translation
	focus_pos = Vector3(translation.x - 0.16, translation.y + 0.1, translation.z + 0.1)

func shoot():
	
	var new_gun_trail = preload("res://scenes/gun_trail.tscn").instance()
	get_node("/root/world").add_child(new_gun_trail)
	new_gun_trail.global_transform = $bullet_anchor.global_transform
	get_node("/root/world").create_trail($bullet_anchor.global_transform)
	
	if $bullet_anchor/RayCast.is_colliding():
		get_node("/root/world").damage_player( int( $bullet_anchor/RayCast.get_collider().get_parent().get_name() ) )
		get_parent().get_parent().hits+=1
	
	if current_pos == default_pos:
		translation.z += throwback
	else:
		translation.z += throwback/5
	
	
	
	rotation_degrees.x += 1
	rotation_degrees.y += rand_range(-5,5)
	rotation_degrees.z += rand_range(-5,5)
	
	
	

func focus():
	pass

func _process(delta):
	
	if Input.is_action_just_pressed("LMB"):
		shoot()
	else:
		pass
	
	if Input.is_action_pressed("RMB"):
		current_pos = focus_pos
	else:
		current_pos = default_pos
	
	translation.x = lerp(translation.x, current_pos.x, 0.1)
	translation.y = lerp(translation.y, current_pos.y, 0.1)
	translation.z = lerp(translation.z, current_pos.z, 0.1)
	rotation_degrees.x = lerp(rotation_degrees.x, 0, 0.1)
	rotation_degrees.y = lerp(rotation_degrees.y, 0, 0.1)
	rotation_degrees.z = lerp(rotation_degrees.z, 0, 0.1)
	
	

	
